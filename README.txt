MI-PAA Readme file
==================

Task 1: Knapsack problem
------------------------

Solving KP with bruteforce and heuristic function. Programmed in Python 3.3.

task1/inst/ - folder with instances, each file has 50 instances on each line ( ID N M item1_weight item1_cost ... itemN_weight itemN_cost)
task1/raspberry_measure/ - output from solving problem on raspberry in ods format
task1/sol/ - folder with solution files
task1/instance.py - main script for solving KP, use instance.py -h for display help message
task1/runner.sh - bash script to measure all instances in inst folder, used on raspberry with Python >= 3.3 (py)