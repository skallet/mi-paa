#!/usr/bin/python

import sys

# Iterate throw file and find instance with given id, return calculated metrics
# @return array [ maximum weight , maximum cost ]
def findSolution ( instanceId , file ):
  try:
    fileHandler = open(file, 'r')
  except IOError:
    print ('Error while opening solution file ' , file)
    sys.exit(2)

  weight = 0
  cost = 0

  for line in fileHandler:
    arguments = line.split(' ');
    id = arguments[0]

    if (id == instanceId):
      weight = int(arguments[1])
      cost = int(arguments[2])
      break

  fileHandler.close()

  return [ weight , cost ]
