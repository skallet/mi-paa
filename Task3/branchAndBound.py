#!/usr/bin/python

class BranchAndBound:
  maxCost = 0
  maxWeight = 0
  countItems = 0
  capacity = 0
  items = []

  def __init__ ( self, N, M, items ):
    self.countItems = N
    self.capacity = M
    self.items = items

  def solve ( self ):
    self._solvePart ( 0, 1 )
    self._solvePart ( 0, 0 )

    return [ self.maxWeight, self.maxCost ]

  def _solvePart ( self, depth, mask ):
    actualValues = self._calculateActualValues( depth, mask )
    maxCost = self._calculateMaxCost ( depth )

    if ( depth >= self.countItems - 1 ):
      if (actualValues[0] <= self.capacity):
        if (actualValues[1] > self.maxCost):
          self.maxCost = actualValues[1]
          self.maxWeight = actualValues[0]
    else:
      #determin if we should continue
      if (actualValues[0] > self.capacity):
        return

      if (actualValues[1] + maxCost <= self.maxCost):
        return
      else:
        depth += 1
        newMask = mask | (1 << depth)

        self._solvePart(depth, newMask)
        self._solvePart(depth, mask)

  def _calculateActualValues ( self, depth, mask ):
    actualCost = 0
    actualWeight = 0

    for itemIndex in range (0, depth + 1):
      position = itemIndex * 2
      binaryIndex = 1 << itemIndex

      if (binaryIndex & mask == binaryIndex):
        item = self.items[position:position+2]

        actualCost += int(item[1])
        actualWeight += int(item[0])

    return [ actualWeight, actualCost ]

  def _calculateMaxCost ( self, depth ):
    maxCost = 0

    for itemIndex in range (depth, self.countItems):
      position = itemIndex * 2
      item = self.items[position:position+2]
      maxCost += int(item[1])

    return maxCost

# Branch and bound solving for one instance
# @return array [ maximum weight , maximum cost ]
def branchAndBound ( N , M , items ):
  itemList = []
  for itemIndex in range(0, N):
    # parse actual item from all items
    position = itemIndex * 2
    item = items[position:position+2]
    item[0] = int(item[0])
    item[1] = int(item[1])
    itemList.append(item)

  itemSorted = []
  for item in sorted(itemList, key = lambda item: -item[1]):
    itemSorted.append(item[0])
    itemSorted.append(item[1])

  solver = BranchAndBound ( N, M, itemSorted )
  return solver.solve()
