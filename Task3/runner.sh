#!/bin/bash

FILES=inst/*
for f in $FILES
do
  echo "Processing $f file..."

	echo "py instance.py -i $f -o ou$f.heuristic.out -c heuristic"
  py instance.py -i $f -o ou$f.heuristic.out -c heuristic
  echo "py instance.py -i $f -o ou$f.bab.out -c bAb"
  py instance.py -i $f -o ou$f.bab.out -c bAb
  echo "py instance.py -i $f -o ou$f.dyn.out -c dynamic"
  py instance.py -i $f -o ou$f.dyn.out -c dynamic
  echo "py instance.py -i $f -o ou$f.fptas.out -c fptas10"
  py instance.py -i $f -o ou$f.fpt10.out -c fptas10
done
