#!/bin/bash

FILES=inst/*
for f in $FILES
do
  echo "Processing $f file..."

  echo "py instance.py -i $f -o ou$f.bab.out --no-brute -s /home/pi/mipaa/task2/sol -c bAb"
  py instance.py -i $f -o ou$f.bab.out --no-brute -s /home/pi/mipaa/task2/sol -c bAb
  echo "py instance.py -i $f -o ou$f.dyn.out --no-brute -s /home/pi/mipaa/task2/sol -c dynamic"
  py instance.py -i $f -o ou$f.dyn.out --no-brute -s /home/pi/mipaa/task2/sol -c dynamic
  echo "py instance.py -i $f -o ou$f.fptas.out --no-brute -s /home/pi/mipaa/task2/sol -c fptas5"
  py instance.py -i $f -o ou$f.fpt5.out --no-brute -s /home/pi/mipaa/task2/sol -c fptas5
  echo "py instance.py -i $f -o ou$f.fptas.out --no-brute -s /home/pi/mipaa/task2/sol -c fptas10"
  py instance.py -i $f -o ou$f.fpt10.out --no-brute -s /home/pi/mipaa/task2/sol -c fptas10
done
