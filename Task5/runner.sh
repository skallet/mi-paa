#!/bin/bash

FILES=instBig/*
for f in $FILES
do

  #echo "File $f (init: 10, population: 20, cross: 50, mutation: 10, generations: 10)"
  py instance.py -i $f -g 10 -p 20 -c 50 -m 10 -n 10

  #echo "File $f (init: 10, population: 20, cross: 50, mutation: 10, generations: 20)"
  py instance.py -i $f -g 10 -p 20 -c 50 -m 10 -n 20

  #echo "File $f (init: 10, population: 20, cross: 50, mutation: 10, generations: 40)"
  py instance.py -i $f -g 10 -p 20 -c 50 -m 10 -n 40

  #echo "File $f (init: 10, population: 20, cross: 50, mutation: 10, generations: 80)"
  py instance.py -i $f -g 10 -p 20 -c 50 -m 10 -n 80

  #echo "File $f (init: 10, population: 20, cross: 50, mutation: 10, generations: 100)"
  py instance.py -i $f -g 10 -p 20 -c 50 -m 10 -n 100

  #echo "File $f (init: 10, population: 20, cross: 50, mutation: 20, generations: 10)"
  py instance.py -i $f -g 10 -p 20 -c 50 -m 20 -n 10
  #echo "File $f (init: 10, population: 20, cross: 50, mutation: 50, generations: 10)"
  py instance.py -i $f -g 10 -p 20 -c 50 -m 50 -n 10

  #echo "File $f (init: 10, population: 20, cross: 50, mutation: 70, generations: 10)"
  py instance.py -i $f -g 10 -p 20 -c 50 -m 70 -n 10

  #echo "File $f (init: 10, population: 20, cross: 50, mutation: 100, generations: 10)"
  py instance.py -i $f -g 10 -p 20 -c 50 -m 100 -n 10

  #echo "File $f (init: 10, population: 40, cross: 50, mutation: 10, generations: 10)"
  py instance.py -i $f -g 10 -p 40 -c 50 -m 10 -n 10

  #echo "File $f (init: 10, population: 80, cross: 50, mutation: 10, generations: 10)"
  py instance.py -i $f -g 10 -p 80 -c 50 -m 10 -n 10

  #echo "File $f (init: 10, population: 120, cross: 50, mutation: 10, generations: 10)"
  py instance.py -i $f -g 10 -p 120 -c 50 -m 10 -n 10

  #echo "File $f (init: 5, population: 20, cross: 50, mutation: 10, generations: 10)"
  py instance.py -i $f -g 5 -p 20 -c 50 -m 10 -n 10

  #echo "File $f (init: 15, population: 20, cross: 50, mutation: 10, generations: 10)"
  py instance.py -i $f -g 15 -p 20 -c 50 -m 10 -n 10

  #echo "File $f (init: 10, population: 20, cross: 25, mutation: 10, generations: 10)"
  py instance.py -i $f -g 10 -p 20 -c 25 -m 10 -n 10

  #echo "File $f (init: 10, population: 20, cross: 75, mutation: 10, generations: 10)"
  py instance.py -i $f -g 10 -p 20 -c 75 -m 10 -n 10

done
