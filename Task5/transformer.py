#!/usr/bin/python

import sys

# 1) what is dependency on n


inputFile = "./sol/output.log"
outputFile = "./sol/cleared.log"

try:
  inputFileHandler = open(inputFile, 'r')
  outputFileHandler = open(outputFile, 'w')
except IOError:
    print ('Error: Input file not found or is not readable')
    sys.exit(2)

for line in inputFileHandler:
  # 0 - measure number
  # 1 - n
  # 2 - c
  # 3 - max weight
  # 4 - IP
  # 5 - P
  # 6 - cross
  # 7 - mutation
  # 8 - generations
  # 9 - best
  # 10 - time

  arguments = line.split(";")

  if (arguments[1] == arguments[2] and arguments[4] == "10" and arguments[5] == "20" and arguments[6] == "50" and arguments[7] == "10" and arguments[8] == "10"):
    outputFileHandler.write(line)

outputFileHandler.close()
inputFileHandler.close()
