#!/bin/bash

FILES=instVeryBig/*
for f in $FILES
do

  #echo "File $f (init: 10, population: 100, cross: 50, mutation: 50, generations: 2000)"
  py instanceProgress.py -i $f -g 10 -p 100 -c 50 -m 50 -n 2000

done
