#!/usr/bin/python

import sys
import getopt
import time
import ntpath
import math
import random

class SAT:
  n = 0
  c = 0
  w = []
  clauses = []

  def __init__ (self, n, c, w, clauses):
    self.n = n
    self.c = c
    self.w = w
    self.clauses = clauses

  def print (self):
    print ("3SAT problem with", self.n, "variables and", self.c, "clauses")
    print ("Weight for variables are", self.w)
    print ("Clauses: ", self.clauses)

  def checkSolution (self, solution):
    if (len(solution) != self.n):
      print ("There is not enought variables in solution!")
      return False

    for i in range(0, self.c):
      clause = 0
      for y in range(0, 3):
        x = self.clauses[i][y]
        pos = int(math.fabs(x)) - 1
        value = solution[pos] if (x > 0) else (0 if solution[pos] == 1 else 1)
        clause |= value
      if (clause == 0):
        return False

    return True

  def getSolutionWeight (self, solution):
    if (self.checkSolution(solution) == False):
      return 0

    weights = [solution[i] * self.w[i] for i in range(0, self.n)]
    return sum(weights)

class Chromozome:
  chromozome = []
  fitness = 0
  relativeFitness = 0

  def __init__ (self, chromozome):
    self.chromozome = chromozome

  def print (self):
    print ("Chromozome:", self.chromozome, "fitness=", self.fitness, "relative fitness=", self.relativeFitness)

class Population:
  chromozomes = []
  sat = 0
  size = 0
  initSize = 0
  cross = 0
  mutation = 0

  def __init__ (self, sat, size, initSize, crossProbability, mutationProbability):
    self.sat = sat
    self.size = size
    self.initSize = initSize
    self.cross = crossProbability
    self.mutation = mutationProbability

    while (True):
      self.chromozomes = []
      for i in range(0, self.initSize):
        randomSolution = [random.randint(0, 1) for y in range(0, self.sat.n)]
        self.chromozomes.append(Chromozome(randomSolution))
      self.calculateFitness()
      if (self.chromozomes[0].fitness != 0):
        break



  def print (self):
    print ("Population: size", self.size, "n", self.sat.n, "init size", self.initSize, "cross", self.cross, "mutation", self.mutation)
    for chromozome in self.chromozomes:
      chromozome.print()

  def calculateFitness (self):
    totalFitness = 0
    for chromozome in self.chromozomes:
      fitness = self.sat.getSolutionWeight(chromozome.chromozome)
      chromozome.fitness = fitness
      totalFitness += fitness

    self.chromozomes = sorted(self.chromozomes, key=lambda chromozome: -chromozome.fitness)
    acc = 0

    if totalFitness == 0:
      totalFitness = 1

    for chromozome in self.chromozomes:
      acc += (chromozome.fitness) / totalFitness
      chromozome.relativeFitness = acc

    self.chromozomes[-1].relativeFitness = 1.0

  def cutPopulation (self):
    uniqueChromozomes = []

    for chromozome in self.chromozomes:
      unique = True
      for u in uniqueChromozomes:
        if (u.chromozome == chromozome.chromozome):
          unique = False
          break
      if unique:
        uniqueChromozomes.append(chromozome)

    uniqueChromozomes = uniqueChromozomes[0:self.initSize]
    self.chromozomes = uniqueChromozomes

  def populate (self):
    self.cutPopulation()
    self.calculateFitness()
    while (len(self.chromozomes) != self.size):
      rulete = random.random()
      parentA = 0
      for chromozome in self.chromozomes:
        if (rulete <= chromozome.relativeFitness):
          parentA = chromozome
          break


      parentB = parentA
      tries = 0
      while (parentB == parentA and len(self.chromozomes) > 1):
          tries += 1
          rulete = random.random()
          if (tries > 100):
            break

          for chromozome in self.chromozomes:
            if (rulete <= chromozome.relativeFitness):
              parentB = chromozome
              break

      sol = []
      for i in range(0, self.sat.n):
        cross = random.randint(0, 100)
        sol.append(parentA.chromozome[i] if cross <= self.cross else parentB.chromozome[i])

      mutation = random.randint(0, 100)
      if (mutation <= self.mutation):
        index = random.randint(0, self.sat.n - 1)
        sol[index] = 0 if sol[index] else 1

      self.chromozomes.append(Chromozome(sol))

    self.calculateFitness()
    return self.chromozomes[0].fitness


def main( argv ):
  inputFile = ''
  initialPopulationSize = 10
  populationSize = 20
  crossProbability = 50
  mutationProbability = 10
  numberOfGenerations = 10

  # load arguments
  try:
    opts, args = getopt.getopt(argv, "hi:g:c:m:n:p:", ["help"])
  except getopt.GetoptError:
    print ('Usage: instance.py -h : for display help message')
    sys.exit(2)

  for opt, arg in opts:
    if opt in ('-h', '--help'):
      print ('Usage: 3SAT solver')
      print ('\t-h : for display this help message')
      print ('\t-i <input_file> : setting input file with 3SAT problem')
      print ('\t-g <number = 10> : size of initial population (> 0)')
      print ('\t-p <number = 20> : size of all population (>= g)')
      print ('\t-c <number = 50> : crossing probability, 0 => all gens from parent A, 100 => all gens from parent B (c = [0; 100])')
      print ('\t-m <number = 10> : probability of mutation for new chromozomes (m = [0; 100])')
      print ('\t-n <number = 10> : number of total generations (n >= 1)')
      sys.exit()
    elif opt in ('-i'):
      inputFile = arg
    elif opt in ('-g'):
      initialPopulationSize = int(arg)
    elif opt in ('-p'):
      populationSize = int(arg)
    elif opt in ('-c'):
      crossProbability = int(arg)
    elif opt in ('-m'):
      mutationProbability = int(arg)
    elif opt in ('-n'):
      numberOfGenerations = int(arg)

  if (initialPopulationSize <= 0):
    print ('Initial population size must be greater than zero, use --help for display help message.')
    sys.exit()

  if (populationSize < initialPopulationSize):
    print ('Population size must be at least as initial population size, use --help for display help message.')
    sys.exit()

  if (crossProbability < 0 or crossProbability > 100):
    print ('Cross probability has to be between 0 and 100, use --help for display help message.')
    sys.exit()

  if (mutationProbability < 0 or mutationProbability > 100):
    print ('Mutation probability has to be between 0 and 100, use --help for display help message.')
    sys.exit()

  if (numberOfGenerations < 1):
    print ('Number of generations must be at least 1, use --help for display help message.')
    sys.exit()

  try:
    inputFileHandler = open(inputFile, 'r')
  except IOError:
    print ('Error: Input file not found or is not readable')
    sys.exit(2)

  # getting instance in file
  lines = [line.rstrip() for line in inputFileHandler]
  arguments = lines[0].split(' ')
  n = int(arguments[2])
  c = int(arguments[3])
  w = [int(num) for num in (lines[1].split(' '))[1:]]
  clauses = []

  for i in range(0, c):
    clause = [int(num) for num in (lines[i + 2].split(' '))[0:3]]
    clauses.append(clause)

  totalTime = 0
  bestResult = 0

  for y in range(0, 3):
    start = time.process_time()
    instance = SAT(n, c, w, clauses)
    population = Population(instance, populationSize, initialPopulationSize, crossProbability, mutationProbability)
    for i in range(0, numberOfGenerations):
      result = population.populate()
      if (result > bestResult):
        bestResult = result
    elapsed = time.process_time() - start
    totalTime += elapsed

  delimiter = ";"

  num = (inputFile.split('_')[1])
  maxWeight = (inputFile.split('_')[4])
  print (num, delimiter, n, delimiter, c, delimiter, maxWeight, delimiter, initialPopulationSize, delimiter, populationSize, delimiter, crossProbability, delimiter, mutationProbability, delimiter, numberOfGenerations, delimiter, bestResult, delimiter, repr(totalTime / 3))

  # clearing after running
  inputFileHandler.close()

if __name__ == "__main__":
  main(sys.argv[1:])
