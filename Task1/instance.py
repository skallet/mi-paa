#!/usr/bin/python

import sys
import getopt
import time
import ntpath
from bruteForce import bruteForce
from heuristic import heuristic
from solutions import findSolution

# Main function
def main ( argv ):
	inputFile = ''
	outputFile = ''
	solutionFile = ''
	solutionFolder = 'sol'
	bruteForceCalc = 1

	# load arguments
	try:
		opts, args = getopt.getopt(argv, "hi:o:s:", ["help", "no-brute"])
	except getopt.GetoptError:
		print ('Usage: instance.py -h : for display help message')
		sys.exit(2)

	# assign arguments into values
	for opt, arg in opts:
		if opt in ('-h', '--help'):
			print ('Usage: instance.py')
			print ('\t-h : for display this help message')
			print ('\t-i <input_file> : setting input file with instances')
			print ('\t-o <output_file> : setting output file for results')
			print ('\t--no-brute : dissable brute force calculation, use solution folder to find solution for relative errors')
			print ('\t-s <solution_folder> : set folder with solution, default is sol');
			sys.exit()
		elif opt in ('-i'):
			inputFile = arg
		elif opt in ('-o'):
			outputFile = arg
		elif opt in ('--no-brute'):
			bruteForceCalc = 0
		elif opt in ('-s'):
			solutionFolder = arg

	# check if input file is set and exist
	try:
		inputFileHandler = open(inputFile, 'r');
	except IOError:
		print ('Error: Input file not found or is not readable')
		sys.exit(2)

	if (bruteForceCalc == 0):
		basename = ntpath.basename(inputFile).replace('.inst.', '.sol.')
		solutionFile = solutionFolder + '/' + basename
		try:
			solutionFileHandler = open(solutionFile, 'r')
			solutionFileHandler.close()
		except IOError:
			print ('Error: Solution file is not readable ', solutionFile)
			sys.exit(2)

	# initialize ouput file, if isset
	outputFileHandler = 0
	if len(outputFile) != 0:
		try:
			outputFileHandler = open(outputFile, 'w')
		except IOError:
			print ('Error: Ouput file is not writable')
			sys.exit(2)

	# getting instances in file
	for line in inputFileHandler:
		arguments = line.split(' ');
		instanceId = arguments[0]
		instanceN = int(arguments[1])
		instanceM = int(arguments[2])
		instanceItems = arguments[3:]

		elapsedTimeBF = 0

		# determin if we need to calculate brute force method, or retrive solution from file
		if (bruteForceCalc == 1):
			# calculate instance with brute force, measure time
			start = time.process_time()
			values = bruteForce(instanceN, instanceM, instanceItems)
			elapsedTimeBF = time.process_time() - start
		else:
			values = findSolution(instanceId, solutionFile)

		# calculate instance with heuristic function, measure time
		start = time.process_time()
		heuristicValues = heuristic(instanceN, instanceM, instanceItems)
		elapsedTimeH = time.process_time() - start

		# compute relative error by formula (C(opt) - C(heur)) / C(opt)
		# opt - optimal solution
		# heur - solution calculated by heuristic function
		relativeError = ( values[1] - heuristicValues[1] ) / values[1]

		# output to file or console
		outputString = instanceId + " " + str(values) + " BruteForce: " + repr(elapsedTimeBF) + " Heuristic: " + repr(elapsedTimeH) + " RelativeError: " + repr(relativeError)
		if (outputFileHandler):
			outputFileHandler.write(outputString + "\n");
		else:
			print (outputString)

	inputFileHandler.close()
	if outputFileHandler:
		outputFileHandler.close()

	sys.exit()


if __name__ == "__main__":
	main(sys.argv[1:])
