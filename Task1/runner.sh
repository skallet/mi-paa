#!/bin/bash

FILES=inst/*
for f in $FILES
do
  echo "Processing $f file..."
  echo "py instance.py -i $f -o $f.nbf.out --no-brute -s /home/pi/mipaa/task1/sol"
  py instance.py -i $f -o $f.nbf.out --no-brute -s /home/pi/mipaa/task1/sol
done