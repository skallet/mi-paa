#!/usr/bin/python

# Heuristic function for calculating maximum cost with given maximum weight
# @return array [ maximum weight , maximum cost ]
def heuristic ( N, M, items ):
  # calculate heuristic value for all items
  itemList = []
  for itemIndex in range(0, N):
    # parse actual item from all items
    position = itemIndex * 2
    item = items[position:position+2]
    item[0] = int(item[0])
    item[1] = int(item[1])
    item.extend( [ item[1] / item[0] ] )
    itemList.append(item)

  # sort items by heuristic value
  itemList = sorted(itemList, key = lambda item: item[2])
  actualCost = 0
  actualWeight = 0

  # iterate throw all items and calculate cost
  for i in reversed(itemList):
    if (actualWeight + i[0] < M):
      actualWeight += i[0]
      actualCost += i[1]

  return [ actualWeight, actualCost ]
