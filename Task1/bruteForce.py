#!/usr/bin/python

# Brute force solving for one instance
# @return array [ maximum weight , maximum cost ]
def bruteForce ( N , M , items ):
  # print ("Solving instance with ", N ," items and maximum weight ", M)
  maximumCost = 0
  maximumWeight = 0

  for index in range(1, 2 ** N):
    # print ("Iteration ", index)
    actualCost = 0
    actualWeight = 0

    for itemIndex in range(0, N):
      # parse only actual item, format: weight cost
      position = itemIndex * 2
      binaryIndex = 1 << itemIndex

      # determin if actual item is used
      if (binaryIndex & index == binaryIndex):
        item = items[position:position+2]

        actualCost += int(item[1])
        actualWeight += int(item[0])

        # determin if next computation is not wasting time
        if (actualWeight > M):
          break

    # store maximum values if necessary
    if (actualWeight <= M):
      if (actualCost > maximumCost):
        maximumCost = actualCost
        maximumWeight = actualWeight

  # return computed values
  return [ maximumWeight, maximumCost ]
