#!/usr/bin/python
import sys
import random

def randomChromozome (length):
  chromozome = []
  temp = ''.join(random.choice('01') for _ in range(length))
  for i, char in enumerate(temp):
    chromozome.append(int(char))

  return chromozome

def fitness (chromozome, items, length, maxWeight):
  weight = 0
  cost = 0

  for i in range(0, length):
    if (chromozome[i]):
      item = items[i:i+2]
      weight += int(items[0])
      cost += int(items[1])

  if (weight > maxWeight):
    cost = 0

  return [weight, cost]

def getRandomElement(population, selfIndex):
  if (len(population)):
    total = 0
    for chromozome in population:
      total += chromozome[0][1]

    randomSibling = random.randint(0, total + 1)
    for i, chromozome in enumerate(population):
      if (i == selfIndex):
        continue
      randomSibling -= chromozome[0][1]
      if (randomSibling <= 0):
        return population.pop(i)

  return

# Generic algorith for calculating maximum cost with given maximum weight
# @return array [ maximum weight , maximum cost ]
def generic ( N, M, items, generations = 10, populationSize = 100 ):
  population = []

  while (len(population) < populationSize):
    chromozome = randomChromozome(N)
    fitnessVal = fitness(chromozome, items, N, M)
    if (fitnessVal[0] <= M):
      population.append([fitnessVal, chromozome])

  for generation in range(0, generations):
    # print ("Generation " + str(generation) + " size of " + str(len(population)))
    # sort chromozomes by cost descending
    population = sorted(population, key = lambda item: item[0][1])
    population = population[-populationSize:]
    nextPopulation = population

    for it in range(0, populationSize):
      parentChromozomeA = population[it]
      parentChromozomeB = getRandomElement(population, it)

      childA = parentChromozomeA
      childB = parentChromozomeB

      if (childB):
        # crossing
        randomCrossing = random.randint(0, N)

        for i in range(randomCrossing, N):
          mem = childB[1][i]
          childB[1][i] = childA[1][i]
          childA[1][i] = mem

        # mutation
        mutationSelect = random.randint(0, 100)
        if (mutationSelect >= 94):
          randomElement = random.randint(0, N - 1)
          childA[1][randomElement] = 0 if childA[1][randomElement] == 1 else 1

        mutationSelect = random.randint(0, 100)
        if (mutationSelect >= 94):
          randomElement = random.randint(0, N - 1)
          childB[1][randomElement] = 0 if childB[1][randomElement] == 1 else 1

        fitnessA = fitness(childA[1], items, N, M)
        fitnessB = fitness(childB[1], items, N, M)

        nextPopulation.append(childA)
        nextPopulation.append(childB)

    population = nextPopulation
    for i, chromozome in enumerate(population):
      population[i][0] = fitness(chromozome[1], items, N, M)

  population = sorted(population, key = lambda item: item[0][1])
  # sys.exit()

  if (len(population)):
    winner = population.pop()
    fit = winner[0]
    return [fit[0],fit[1]];
  else:
    return [0,0]
