#!/usr/bin/python

import sys
import math
from dynamicPrograming import Dynamic

class FPTAS10 ( Dynamic ):
  eps = 0.1
  costMax = 0

  def __init__ ( self, N, M, items ):
    super( FPTAS10, self).__init__( N, M, items )
    for item in self.items:
      self.costMax += item[1]

  def _decomposeTable ( self, items, cost ):
    if ( cost <= 0 and items <= 0 ):
      return 0
    elif ( items == 0 ):
      return float("inf");
    else:
      item = self.items[ self.countItems - items ]

      b = math.floor( math.log( self.eps * self.costMax / self.countItems ) )
      keyCost = cost
      for i in range(b):
        keyCost = keyCost & ~( 1 << i )

      result = []

      try:
        result = self.decompose[items, keyCost]
      except KeyError:
        self.decompose[items, keyCost] = min(
          self._decomposeTable( items - 1, cost ) ,
          self._decomposeTable( items - 1, cost - item[1] ) + item[0]
        )
        result = self.decompose[items, keyCost]

      return result

# FPTAS programing for calculating maximum cost with given maximum weight
# @return array [ maximum weight , maximum cost ]
def fptas10 ( N, M, items ):
  solver = FPTAS10 ( N, M, items )
  return solver.solve()
