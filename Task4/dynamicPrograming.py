#!/usr/bin/python

class Dynamic:
  countItems = 0
  capacity = 0
  items = []
  decompose = {}

  def __init__ ( self, N, M, items ):
    self.countItems = N
    self.capacity = M
    self.decompose = {}

    itemList = []
    for i in range ( 0, N ):
      position = i*2
      item = items[ position:position+2 ]
      item[0] = int( item[0] )
      item[1] = int( item[1] )
      itemList.append( item )

    self.items =  itemList

  def solve ( self ):
    # get sum of all costs
    costSum = 0
    for item in self.items:
      costSum += item[1]

    values = []
    # lets find first suitable solution
    for i in range ( costSum , 0, -1 ):
      weight = self._decomposeTable( self.countItems, i )
      if ( weight <= self.capacity ):
        return [ weight, i ]

    return [ 0, 0 ]

  def _decomposeTable ( self, items, cost ):
    if ( cost <= 0 and items <= 0 ):
      return 0
    elif ( items == 0 ):
      return float("inf");
    else:
      item = self.items[ self.countItems - items ]

      result = []

      try:
        result = self.decompose[items, cost]
      except KeyError:
        self.decompose[items, cost] = min(
          self._decomposeTable( items - 1, cost ) ,
          self._decomposeTable( items - 1, cost - item[1] ) + item[0]
        )
        result = self.decompose[items, cost]

      return result

# Dynamic programing for calculating maximum cost with given maximum weight
# @return array [ maximum weight , maximum cost ]
def dynamic ( N, M, items ):
  solver = Dynamic ( N, M, items )
  return solver.solve()
