#!/usr/bin/python

import sys
import getopt
import time
import ntpath
import math
from bruteForce import bruteForce
from heuristic import heuristic
from solutions import findSolution
from branchAndBound import branchAndBound
from dynamicPrograming import dynamic
from fptas5 import fptas5
from fptas10 import fptas10
from generic import generic

# Main function
def main ( argv ):
	inputFile = ''
	outputFile = ''
	solutionFile = ''
	solutionFolder = 'sol'
	bruteForceCalc = 1
	generations = 10
	population = 100

	# default values for calculations methods
	calculation = 'heuristic'
	implementation = ''
	implementedCalcs = ['heuristic', 'bAb', 'dynamic', 'fptas5', 'fptas10', 'generic']
	implementations = [heuristic, branchAndBound, dynamic, fptas5, fptas10, generic]

	# load arguments
	try:
		opts, args = getopt.getopt(argv, "hi:o:s:c:g:p:", ["help", "no-brute"])
	except getopt.GetoptError:
		print ('Usage: instance.py -h : for display help message')
		sys.exit(2)

	# assign arguments into values
	for opt, arg in opts:
		if opt in ('-h', '--help'):
			print ('Usage: instance.py')
			print ('\t-h : for display this help message')
			print ('\t-i <input_file> : setting input file with instances')
			print ('\t-o <output_file> : setting output file for results')
			print ('\t--no-brute : dissable brute force calculation, use solution folder to find solution for relative errors')
			print ('\t-s <solution_folder> : set folder with solution, default is sol')
			print ('\t-c <calculation	: use type of calculation ', implementedCalcs)
			print ('\t-g <number of generations> : set terminating condition for generic algorithm [default value is 10]')
			print ('\t-p <size of population> : set size of population for generic algorithm [default value is 100]')
			sys.exit()
		elif opt in ('-i'):
			inputFile = arg
		elif opt in ('-o'):
			outputFile = arg
		elif opt in ('--no-brute'):
			bruteForceCalc = 0
		elif opt in ('-s'):
			solutionFolder = arg
		elif opt in ('-c'):
			calculation = arg
			if ( calculation not in implementedCalcs ):
				print ('Calculation with ', calculation, ' is not implemented, use -h for help.')
				sys.exit()
		elif opt in ('-g'):
			generations = int(arg)
			if (generations <= 0):
				print ('Number of generations must be greater than zero, use -h for help.')
				sys.exit()
		elif opt in ('-p'):
			population = int(arg)
			if (population <= 0):
				print ('Population size must be greater than zero, use -h for help.')
				sys.exit()

	implementation = implementations[ implementedCalcs.index( calculation ) ]

	# check if input file is set and exist
	try:
		inputFileHandler = open(inputFile, 'r');
	except IOError:
		print ('Error: Input file not found or is not readable')
		sys.exit(2)

	if (bruteForceCalc == 0):
		basename = ntpath.basename(inputFile).replace('.inst.', '.sol.')
		solutionFile = solutionFolder + '/' + basename
		try:
			solutionFileHandler = open(solutionFile, 'r')
			solutionFileHandler.close()
		except IOError:
			print ('Error: Solution file is not readable ', solutionFile)
			sys.exit(2)

	# initialize ouput file, if isset
	outputFileHandler = 0
	if len(outputFile) != 0:
		try:
			outputFileHandler = open(outputFile, 'w')
		except IOError:
			print ('Error: Ouput file is not writable')
			sys.exit(2)

	# getting instances in file
	for line in inputFileHandler:
		arguments = line.split(' ');
		instanceId = arguments[0]
		instanceN = int(arguments[1])
		instanceM = int(arguments[2])
		instanceItems = arguments[3:]

		elapsedTimeBF = 0

		# determin if we need to calculate brute force method, or retrive solution from file
		if (bruteForceCalc == 1):
			# calculate instance with brute force, measure time
			start = time.process_time()
			values = bruteForce(instanceN, instanceM, instanceItems)
			elapsedTimeBF = time.process_time() - start
		else:
			values = findSolution(instanceId, solutionFile)

		# calculate instance with implementation function, measure time
		start = time.process_time()
		if (calculation != 'generic'):
			implementationValues = implementation(instanceN, instanceM, instanceItems)
		else:
			implementationValues = implementation(instanceN, instanceM, instanceItems, generations, population)
		elapsedTimeH = time.process_time() - start

		# compute relative error by formula (C(opt) - C(heur)) / C(opt)
		# opt - optimal solution
		# heur - solution calculated by implementation function
		relativeError = ( values[1] - implementationValues[1] ) / values[1]

		# output to file or console
		outputString = instanceId + " " + str(values) + " BruteForce: " + repr(elapsedTimeBF) + " " + calculation + ": " + repr(elapsedTimeH) + " RelativeError: " + repr( math.fabs(relativeError) )
		if (outputFileHandler):
			outputFileHandler.write(outputString + "\n");
		else:
			print (outputString)

	inputFileHandler.close()
	if outputFileHandler:
		outputFileHandler.close()

	sys.exit()


if __name__ == "__main__":
	main(sys.argv[1:])
