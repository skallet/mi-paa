#!/bin/bash

FILES=inst/*
for f in $FILES
do
  echo "Processing $f file..."

	echo "py instance.py -i $f -o ou$f.out -c generic --no-brute -g 10 -p 50"
  py instance.py -i $f -o ou$f.10.50.out -c generic --no-brute -g 10 -p 50

  echo "py instance.py -i $f -o ou$f.out -c generic --no-brute -g 10 -p 100"
  py instance.py -i $f -o ou$f.10.100.out -c generic --no-brute -g 10 -p 100

  echo "py instance.py -i $f -o ou$f.out -c generic --no-brute -g 10 -p 200"
  py instance.py -i $f -o ou$f.10.200.out -c generic --no-brute -g 10 -p 200

  echo "py instance.py -i $f -o ou$f.out -c generic --no-brute -g 5 -p 50"
  py instance.py -i $f -o ou$f.5.50.out -c generic --no-brute -g 5 -p 50

  echo "py instance.py -i $f -o ou$f.out -c generic --no-brute -g 20 -p 50"
  py instance.py -i $f -o ou$f.20.50.out -c generic --no-brute -g 20 -p 50
done
